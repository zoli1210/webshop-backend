package com.webshop.webshopfullstack.service;


import com.webshop.webshopfullstack.persistence.RegistrationRepository;
import com.webshop.webshopfullstack.persistence.entity.RegistrationEntity;
import com.webshop.webshopfullstack.service.domain.Registration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.security.SecureRandom;
import java.util.Base64;


public class RegistrationServiceTest {

    private RegistrationService registrationService;
    private RegistrationRepository registrationRepository;

    @BeforeEach
    void setUp() {
        registrationRepository = Mockito.mock(RegistrationRepository.class);
        registrationService = new RegistrationService(registrationRepository, new SecureRandom(), Base64.getUrlEncoder());
    }

    @Test
    void testInvalidRegistrationMissingValues() {

        //given
        final var registrationData = new Registration(
                null,
                "Szabo",
                "Laszlo",
                "szabolaszlo@gmail.com",
                "asdasd"
        );

        Mockito.when(registrationRepository.save(Mockito.any(RegistrationEntity.class)))
                .then(it -> it.getArgument(0, RegistrationEntity.class));
        //when
        registrationService.createUser(registrationData);

        //then

        var captor = ArgumentCaptor.forClass(RegistrationEntity.class);
        Mockito.verify(registrationRepository, Mockito.times(1)).save(captor.capture());

        var registrationEntity = captor.getValue();

        Assertions.assertNotNull(registrationEntity.getCustomerId());
        Assertions.assertEquals("Szabo", registrationEntity.getFirstName());
        Assertions.assertEquals("Laszlo", registrationEntity.getLastName());
        Assertions.assertEquals("szabolaszlo@gmail.com", registrationEntity.getEmail());
        Assertions.assertEquals("37402464979f6b26bde6ccef54de4f3733c1ba58ad8d0bdc5659ae4b946a397a4a2bd102ef154957940ba30e1a497787352a569d0909871927b7bffcc248e996", registrationEntity.getPassword());
    }
}
