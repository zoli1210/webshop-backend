package com.webshop.webshopfullstack.service;

import com.webshop.webshopfullstack.persistence.OrderRepository;
import com.webshop.webshopfullstack.persistence.RegistrationRepository;
import com.webshop.webshopfullstack.persistence.entity.OrderEntity;
import com.webshop.webshopfullstack.persistence.entity.OrderItemEntity;
import com.webshop.webshopfullstack.service.domain.Category;
import com.webshop.webshopfullstack.service.domain.Order;
import com.webshop.webshopfullstack.service.domain.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;


import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

public class OrderServiceTest {

    private OrderService orderService;
    private ProductService productService;
    private OrderRepository orderRepository;
//     registrationRepository;

    @BeforeEach
    void setUp() {
        productService = Mockito.mock(ProductService.class);
        orderRepository = Mockito.mock(OrderRepository.class);
        orderService = new OrderService(orderRepository, productService);
    }

    @Test()
    void testInvalidOrderSubmission_missingValues() throws Exception {
        //given
        final var orderItems = List.of(new Order.Item(1L, 10, "name", "url", 10));
        final var orderSubmission = new Order(null,
                "Laszlo",
                "Szabo",
                "szabolaszlo@gmail.com",
                "Hungary",
                "3525",
                "Miskolc",
                "Kis-Hunyad utca 20 3/3",
                "",
                "162kvn59als9d",
                80,
                6,
                "PAYPAL",
                orderItems);

        Mockito.when(productService.getProductById(any(Long.class)))
                .thenReturn(Optional.empty());

        //when
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Order result = orderService.createOrder(orderSubmission);
        }, "Order products are empty");
    }

    @Test()
    void testValidOrderSubmission() throws Exception {
        //given
        final var orderItemsEntitySent = List.of(new OrderItemEntity(1L, "name", "url", 10, 10));
        final var orderEntitySent = new OrderEntity("765a71bf-11f7-4ca7-a415-022879e9dfc0",
                "Laszlo",
                "Szabo",
                "szabolaszlo@gmail.com",
                "Hungary",
                "3525",
                "Miskolc",
                "Kis-Hunyad utca 20 3/3",
                "",
                "162kvn59als9d",
                80,
                6,
                "PAYPAL",
                orderItemsEntitySent);
        final var orderItems = List.of(new Order.Item(1L, 10, "name", "url", 10));
        final var orderSubmission = new Order(null,
                "Laszlo",
                "Szabo",
                "szabolaszlo@gmail.com",
                "Hungary",
                "3525",
                "Miskolc",
                "Kis-Hunyad utca 20 3/3",
                "",
                "162kvn59als9d",
                80,
                6,
                "PAYPAL",
                orderItems);
        final var product = new Product(
                1L,
                Category.COFFEE_MUGS,
                "COFFEEMUG-1020",
                "Coffee Mug - Relax",
                "Do you love mathematics? If so, then you need this elegant coffee mug with an amazing fractal design. You dont have to worry about boring coffee mugs anymore",
                14,
                "assets/images/productModels/coffeemugs/coffeemug-luv2code-1020.png",
                true,
                100,
                Date.valueOf(LocalDate.of(2020, 1, 1)),
                Date.valueOf(LocalDate.of(2020, 1, 1))
        );
        Mockito.when(productService.getProductById(1))
                .thenReturn(Optional.of(product));
        Mockito.when(orderRepository.save(Mockito.any(OrderEntity.class)))
                .thenReturn(orderEntitySent);
//                .then(it -> it.getArgument(0, OrderEntity.class));

        //when
        orderService.createOrder(orderSubmission);

        //then
        var captor = ArgumentCaptor.forClass(OrderEntity.class);
        Mockito.verify(orderRepository, Mockito.times(1)).save(captor.capture());

        var orderEntity = captor.getValue();

        Assertions.assertEquals("Laszlo", orderEntity.getFirstName());
        Assertions.assertEquals("Szabo", orderEntity.getLastName());
        Assertions.assertEquals("szabolaszlo@gmail.com", orderEntity.getEmail());
        Assertions.assertEquals("Hungary", orderEntity.getCountry());
        Assertions.assertEquals("3525", orderEntity.getZipCode());
        Assertions.assertEquals("Miskolc", orderEntity.getCity());
        Assertions.assertEquals("Kis-Hunyad utca 20 3/3", orderEntity.getStreetAndHouseNumber());
        Assertions.assertEquals("", orderEntity.getOther());
        Assertions.assertEquals("162kvn59als9d", orderEntity.getCustomerId());
        Assertions.assertEquals(80, orderEntity.getTotalPrice());
        Assertions.assertEquals(6, orderEntity.getTotalQuantity());
        Assertions.assertEquals("PAYPAL", orderEntity.getPaymentMethod());


        var items = orderEntity.getOrderItems();
        Assertions.assertEquals(1, items.size());

        var actualItem = items.get(0);

        Assertions.assertEquals(0, actualItem.getId(), "Item ID does not math");
        Assertions.assertEquals(10, actualItem.getQuantity());
        Assertions.assertEquals("Coffee Mug - Relax", actualItem.getName());
        Assertions.assertEquals("assets/images/productModels/coffeemugs/coffeemug-luv2code-1020.png", actualItem.getImageUrl());
        Assertions.assertEquals(14, actualItem.getUnitPrice());
    }
}