create table if not exists ORDERS
(
    ID                      bigint not null AUTO_INCREMENT,
    CITY                    varchar(255) default null,
    COUNTRY                 varchar(255) default null,
    CREATED_AT              timestamp(6) default null,
    CUSTOMER_ID             varchar(255) default null,
    EMAIL                   varchar(255) default null,
    FIRST_NAME              varchar(255) default null,
    LAST_NAME               varchar(255) default null,
    ORDER_TRACKING_NUMBER   varchar(255) default null,
    OTHER                   varchar(255) default null,
    PAYMENT_METHOD          varchar(255) default null,
    STREET_AND_HOUSE_NUMBER varchar(255) default null,
    TOTAL_PRICE             int    not null,
    TOTAL_QUANTITY          int    not null,
    UPDATED_AT              timestamp(6) default null,
    ZIP_CODE                varchar(255) default null,
    primary key (ID)
)
    ENGINE=InnoDB
AUTO_INCREMENT = 1;

create table if not exists ORDER_ITEMS
(
    ID          bigint not null AUTO_INCREMENT,
    IMAGE_URL   varchar(255) default null,
    NAME        varchar(255) default null,
    QUANTITY    int    not null,
    UID         bigint not null,
    UNIT_PRICE  int    not null,
    ORDER_ITEMS bigint       default null,
    primary key (ID),
    constraint FK5OY8487QARP952C4KI6QUSWF8
        foreign key (ORDER_ITEMS)
            references ORDERS (ID),
    index       FK5OY8487QARP952C4KI6QUSWF8 (ORDER_ITEMS)
)
    ENGINE=InnoDB
    AUTO_INCREMENT = 1;
create table if not exists REGISTRATION
(
    ID           bigint not null AUTO_INCREMENT,
    ACCESS_TOKEN varchar(255) default null,
    CUSTOMER_ID  varchar(255) default null,
    EMAIL        varchar(255) default null,
    FIRST_NAME   varchar(255) default null,
    LAST_NAME    varchar(255) default null,
    PASSWORD     varchar(255) default null,
    primary key (ID)
)
    ENGINE=InnoDB
    AUTO_INCREMENT = 1;
create table if not exists PRODUCT_CATEGORY
(
    ID            bigint not null AUTO_INCREMENT,
    CATEGORY_NAME varchar(255) default null,
    primary key (ID)
)
    ENGINE=InnoDB
    AUTO_INCREMENT = 1;
create table if not exists PRODUCT
(
    ID             bigint not null AUTO_INCREMENT,
    SKU            varchar(255)   default null,
    NAME           varchar(255)   default null,
    DESCRIPTION    character(511) default null,
    UNIT_PRICE     decimal(13, 2) default null,
    IMAGE_URL      varchar(255)   default null,
    ACTIVE         bit            default true,
    UNITS_IN_STOCK int            default null,
    DATE_CREATED   timestamp(6)   default null,
    CATEGORY_ID    bigint not null,
    LAST_UPDATED   timestamp(6)   default null,
    primary key (ID),
    constraint FK_CATEGORY
        foreign key (CATEGORY_ID)
            references PRODUCT_CATEGORY (ID)
) ENGINE=InnoDB
    AUTO_INCREMENT = 1;