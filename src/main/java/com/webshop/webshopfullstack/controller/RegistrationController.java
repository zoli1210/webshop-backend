package com.webshop.webshopfullstack.controller;

import com.webshop.webshopfullstack.controller.model.RegistrationModel;
import com.webshop.webshopfullstack.persistence.entity.RegistrationEntity;
import com.webshop.webshopfullstack.service.RegistrationService;
import com.webshop.webshopfullstack.service.domain.Registration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class RegistrationController {

    private final RegistrationService registrationService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @PostMapping(value = Api.BASE_PATH + "/registrationData")
    private ResponseEntity<RegistrationModel> saveUser(@RequestBody RegistrationModel registrationModel) throws Exception {
        var createUser = registrationService.createUser(mapToDomain(registrationModel));
        LOGGER.info("Create new user");
        return ResponseEntity.ok(mapToModel(createUser));
    }

    @PostMapping(value = Api.BASE_PATH + "/emailExist")
    public ResponseEntity<Boolean> getEmailExist(@RequestBody String email) {
        Boolean emailExist = registrationService.checkEmailExist(email);
        return ResponseEntity.ok(emailExist);
    }


    private Registration mapToDomain(RegistrationModel registrationModel) {
        return new Registration(
                registrationModel.getCustomerId(),
                registrationModel.getFirstName(),
                registrationModel.getLastName(),
                registrationModel.getEmail(),
                registrationModel.getPassword()
        );
    }

    private RegistrationModel mapToModel(Registration registration) {
        return new RegistrationModel(
                registration.getCustomerId(),
                registration.getFirstName(),
                registration.getLastName(),
                registration.getEmail(),
                registration.getPassword()
        );
    }
}
