package com.webshop.webshopfullstack.controller;

import com.webshop.webshopfullstack.controller.model.OrderModel;
import com.webshop.webshopfullstack.service.OrderService;

import com.webshop.webshopfullstack.service.RegistrationService;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@RestController
public class OrderController {

    private final OrderService orderService;
    private final RegistrationService registrationService;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    public OrderController(OrderService orderService, RegistrationService registrationService) {
        this.orderService = orderService;
        this.registrationService = registrationService;
    }

    @PostMapping(value = Api.BASE_PATH + "/order")
    private ResponseEntity<OrderModel> saveOrder(@RequestBody OrderModel orderModel, HttpServletRequest request) throws Exception {
        var accessToken = request.getHeader("accessToken");
        var email = request.getHeader("email");
        var userIsValid = registrationService.validUser(accessToken, email);
        if (userIsValid) {
            var createdOrder = orderService.createOrder(mapToDomain(orderModel));
            LOGGER.info("User is valid");
            return ResponseEntity.ok(mapToModel(createdOrder));
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    private List<com.webshop.webshopfullstack.service.domain.Order.Item> listMappingToDomain(List<OrderModel.Item> domainItem) {
        List<com.webshop.webshopfullstack.service.domain.Order.Item> itemList = new ArrayList<>();
        for (OrderModel.Item orderItem : domainItem) {
            com.webshop.webshopfullstack.service.domain.Order.Item orderItems = orderItemMapper(orderItem);
            itemList.add(orderItems);
        }
        return itemList;
    }

    private com.webshop.webshopfullstack.service.domain.Order.Item orderItemMapper(OrderModel.Item newDomainItem) {
        return new com.webshop.webshopfullstack.service.domain.Order.Item(
                newDomainItem.getId(),
                newDomainItem.getQuantity()
        );
    }

    private List<OrderModel.Item> listMappingToModel(List<com.webshop.webshopfullstack.service.domain.Order.Item> modelItem) {
        List<OrderModel.Item> itemList = new ArrayList<>();

        for (com.webshop.webshopfullstack.service.domain.Order.Item orderItem : modelItem) {
            OrderModel.Item orderItems = orderItemMappingToModel(orderItem);
            itemList.add(orderItems);
        }
        return itemList;
    }

    private OrderModel.Item orderItemMappingToModel(com.webshop.webshopfullstack.service.domain.Order.Item newModelItem) {
        return new OrderModel.Item(
                newModelItem.getId(),
                newModelItem.getQuantity(),
                newModelItem.getName(),
                newModelItem.getImageUrl(),
                newModelItem.getUnitPrice()
        );
    }

    private com.webshop.webshopfullstack.service.domain.Order mapToDomain(OrderModel domain) {
        return new com.webshop.webshopfullstack.service.domain.Order(

                domain.getOrderTrackingNumber(),
                domain.getFirstName(),
                domain.getLastName(),
                domain.getEmail(),
                domain.getCountry(),
                domain.getZipCode(),
                domain.getCity(),
                domain.getStreetAndHouseNumber(),
                domain.getOther(),
                domain.getCustomerId(),
                domain.getTotalPrice(),
                domain.getTotalQuantity(),
                domain.getPaymentMethod(),
                listMappingToDomain(domain.getOrderItems())
        );
    }

    private OrderModel mapToModel(com.webshop.webshopfullstack.service.domain.Order model) {
        return new OrderModel(

                model.getOrderTrackingNumber(),
                model.getFirstName(),
                model.getLastName(),
                model.getEmail(),
                model.getCountry(),
                model.getZipCode(),
                model.getCity(),
                model.getStreetAndHouseNumber(),
                model.getOther(),
                model.getCustomerId(),
                model.getTotalPrice(),
                model.getTotalQuantity(),
                model.getPaymentMethod(),
                listMappingToModel(model.getOrderItems())
        );
    }
}
