package com.webshop.webshopfullstack.controller;

import com.webshop.webshopfullstack.controller.model.AuthModel;
import com.webshop.webshopfullstack.persistence.entity.RegistrationEntity;
import com.webshop.webshopfullstack.service.AuthService;
import com.webshop.webshopfullstack.service.RegistrationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class AuthController {

    private final AuthService authService;

    private final RegistrationService registrationService;

    public AuthController(AuthService authService, RegistrationService registrationService) {
        this.authService = authService;
        this.registrationService = registrationService;
    }

    @PostMapping(value = Api.BASE_PATH + "/login")
    public ResponseEntity<RegistrationEntity> getAuthController(@RequestBody AuthModel authModel) {
        var checkLoginData = authService.checkValidUser(authModel.getEmail(), authModel.getPassword());

        if (checkLoginData) {
            var registrationEntity = registrationService.addAccessTokenByEmail(authModel.getEmail());
            return ResponseEntity.ok(registrationEntity);

            //*TODO: go to HTTPS
//           Cookie cookie = new Cookie("accessToken", registrationEntity.getAccessToken());
//           cookie.setMaxAge(10800);
//            cookie.setSecure(true);
//            cookie.setHttpOnly(true);
//            response.addCookie(cookie);
//            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.status(401).build();
    }

    @GetMapping(value = Api.BASE_PATH + "/cookieData/{accessToken}")
    public ResponseEntity<Boolean> getCookieToken(@PathVariable String accessToken) {
        Boolean tokenExist = authService.checkCookieData(accessToken);
        return ResponseEntity.ok(tokenExist);
    }
}
