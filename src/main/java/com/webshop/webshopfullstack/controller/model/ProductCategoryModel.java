package com.webshop.webshopfullstack.controller.model;


import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;
import java.util.Set;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class ProductCategoryModel {
    private int id;
    private String categoryName;
    private List<ProductModel> productModels;
}
