package com.webshop.webshopfullstack.controller.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class ProductModel {
    private long id;
    private CategoryModel categoryModel;
    private String sku;
    private String name;
    private String description;
    private int unitPrice;
    private String imageUrl;
    private boolean active;
    private int unitsInStock;
    private Date dateCreated;
    private Date lastUpdated;
}
