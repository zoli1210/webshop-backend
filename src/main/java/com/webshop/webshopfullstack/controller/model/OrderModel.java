package com.webshop.webshopfullstack.controller.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class OrderModel {


    private String orderTrackingNumber;
    private String firstName;
    private String lastName;
    private String email;
    private String country;
    private String zipCode;
    private String city;
    private String streetAndHouseNumber;
    private String other;
    private String customerId;
    private int totalPrice;
    private int totalQuantity;
    private String paymentMethod;
    private List<OrderModel.Item> orderItems;

    @AllArgsConstructor
    @Getter
    public static class Item {

        private long id;
        private int quantity;
        private String name;
        private String imageUrl;
        private int unitPrice;
    }

}
