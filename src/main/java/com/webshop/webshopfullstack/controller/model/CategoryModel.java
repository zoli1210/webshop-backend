package com.webshop.webshopfullstack.controller.model;


import java.util.Arrays;
import java.util.Optional;

public enum CategoryModel {

    BOOKS("Books", 1),
    COFFEE_MUGS("Coffe Mugs", 2),
    MOUSE_PADS("Mouse Pads", 3),
    LUGGAGE_TAGS("Luggage Tags", 4);

    private final String description;
    private final int id;

    CategoryModel(String description, int id) {
        this.description = description;
        this.id = id;
    }

    public static Optional<CategoryModel> ofId(final int id) {
        return Arrays.stream(CategoryModel.values())
                .filter(categoryModel -> categoryModel.id == id)
                .findFirst();
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }
}
