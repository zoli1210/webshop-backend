package com.webshop.webshopfullstack.controller;

import com.webshop.webshopfullstack.controller.model.CategoryModel;
import com.webshop.webshopfullstack.controller.model.ProductModel;
import com.webshop.webshopfullstack.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {

    private final ProductService productService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = Api.BASE_PATH + "/products/{id}")
    private ResponseEntity<ProductModel> getProductsById(@PathVariable int id) {
        LOGGER.info("Fetching product with id {}", id);
        Optional<ProductModel> product = productService.getProductById(id)
                .map(this::mapToModel);
        if (product.isEmpty()) {
            LOGGER.info("Id not found");
            return ResponseEntity.notFound().build();
        }
        LOGGER.info("Id found");
        return ResponseEntity.ok(product.get());
    }

    @GetMapping(value = Api.BASE_PATH + "/products")
    private ResponseEntity<List<ProductModel>> getProduct(@RequestParam(name = "category-id", required = false) Integer categoryId) {
        List<ProductModel> productModels;
        if (categoryId == null) {
            LOGGER.info("Fetching all products");
            productModels = productService.getProducts()
                    .stream()
                    .map(this::mapToModel)
                    .toList();
        } else {
            LOGGER.info("Fetching products by id {}", categoryId);
            productModels = productService.getProductsByCategory(categoryId)
                    .stream()
                    .map(this::mapToModel)
                    .toList();
        }
        return ResponseEntity.ok(productModels);
    }

    private ProductModel mapToModel(com.webshop.webshopfullstack.service.domain.Product domain) {
        return new ProductModel(
                domain.getId(),
                CategoryModel.ofId(domain.getCategory().getId()).orElse(null),
                domain.getSku(),
                domain.getDescription(),
                domain.getName(),
                domain.getUnitPrice(),
                domain.getImageUrl(),
                domain.isActive(),
                domain.getUnitsInStock(),
                domain.getDateCreated(),
                domain.getLastUpdated()
        );
    }

}
