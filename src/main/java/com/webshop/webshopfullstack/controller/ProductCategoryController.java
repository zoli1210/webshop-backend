package com.webshop.webshopfullstack.controller;

import com.webshop.webshopfullstack.controller.model.CategoryModel;
import com.webshop.webshopfullstack.controller.model.ProductCategoryModel;
import com.webshop.webshopfullstack.controller.model.ProductModel;
import com.webshop.webshopfullstack.service.ProductService;
import com.webshop.webshopfullstack.service.domain.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class ProductCategoryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductCategoryController.class);

    private final ProductService productService;

    @Autowired
    public ProductCategoryController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = Api.BASE_PATH + "/product-categories")
    private ResponseEntity<List<ProductCategoryModel>> getProductCategories() {
        var productCategories = productService.getProductsGroupByCategories()
                .entrySet()
                .stream()
                .map(it -> new ProductCategoryModel(it.getKey().getId(), it.getKey().getDescription(),
                                it.getValue()
                                        .stream()
                                        .map(this::mapToModel)
                                        .toList()
                        )
                ).toList();
        LOGGER.debug("Product categories {}", productCategories);
        return ResponseEntity.ok(productCategories);
    }

    @GetMapping(value = Api.BASE_PATH + "/product-categories/{id}")
    private ResponseEntity<ProductCategoryModel> getProductCategory(@PathVariable int id) {
        LOGGER.info("fetching product category with id {}, for user {}", id, null);
        var products = productService.getProductsByCategory(id)
                .stream()
                .map(this::mapToModel)
                .toList();
        final var productCategories = new ProductCategoryModel(
                id,
                CategoryModel.ofId(id)
                        .map(CategoryModel::getDescription)
                        .orElse(null),
                products);
        LOGGER.debug("Product categories {}", productCategories);
        LOGGER.info("Request successful");
        return ResponseEntity.ok(productCategories);
    }

    private ProductModel mapToModel(Product domain) {
        return new ProductModel(
                domain.getId(),
                CategoryModel.ofId(domain.getCategory().getId()).orElse(null),
                domain.getSku(),
                domain.getDescription(),
                domain.getName(),
                domain.getUnitPrice(),
                domain.getImageUrl(),
                domain.isActive(),
                domain.getUnitsInStock(),
                domain.getDateCreated(),
                domain.getLastUpdated()
        );
    }
}
