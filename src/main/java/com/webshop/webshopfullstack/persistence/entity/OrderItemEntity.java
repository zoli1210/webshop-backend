package com.webshop.webshopfullstack.persistence.entity;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "order_items")
@Getter
public class OrderItemEntity {

    public OrderItemEntity() {
    }

    public OrderItemEntity(long uid, String name, String imageUrl, int unitPrice, int quantity) {
        this.uid = uid;
        this.name = name;
        this.imageUrl = imageUrl;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long uid;
    private String name;
    private String imageUrl;
    private int unitPrice;
    private int quantity;
    @ManyToOne
    @JoinColumn(name = "orderItems")
    private OrderEntity orderItems;
}
