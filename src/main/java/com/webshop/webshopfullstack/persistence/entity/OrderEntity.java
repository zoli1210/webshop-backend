package com.webshop.webshopfullstack.persistence.entity;

import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "orders")
@Getter
public class OrderEntity {

    public OrderEntity() {
    }

    public OrderEntity(String orderTrackingNumber,
                       String firstName,
                       String lastName,
                       String email,
                       String country,
                       String zipCode,
                       String city,
                       String streetAndHouseNumber,
                       String other,
                       String customerId,
                       int totalPrice,
                       int totalQuantity,
                       String paymentMethod,
                       List<OrderItemEntity> orderItems
    ) {
        this.orderTrackingNumber = orderTrackingNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.country = country;
        this.zipCode = zipCode;
        this.city = city;
        this.streetAndHouseNumber = streetAndHouseNumber;
        this.other = other;
        this.customerId = customerId;
        this.totalPrice = totalPrice;
        this.totalQuantity = totalQuantity;
        this.paymentMethod = paymentMethod;
        this.orderItems = orderItems;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String orderTrackingNumber;
    private String firstName;
    private String lastName;
    private String email;
    private String country;
    private String zipCode;
    private String city;
    private String streetAndHouseNumber;
    private String other;
    private String customerId;
    private int totalPrice;
    private int totalQuantity;
    @CreationTimestamp
    private Date createdAt;
    @UpdateTimestamp
    private Date updatedAt;
    private String paymentMethod;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orderItems")
    private List<OrderItemEntity> orderItems;

}