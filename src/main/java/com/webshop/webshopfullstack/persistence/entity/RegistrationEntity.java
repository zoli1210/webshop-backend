package com.webshop.webshopfullstack.persistence.entity;

import lombok.Getter;
import lombok.ToString;

import javax.persistence.*;


@Entity
@Table(name = "registration")
@Getter
public class RegistrationEntity {

    public RegistrationEntity() {
    }

    public RegistrationEntity(String customerId, String firstName, String lastName, String email, String password) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    @Id
    @GeneratedValue
    private Long id;
    private String customerId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String accessToken;

    public void setAccessToken(String token) {
        this.accessToken = token;
    }
}
