package com.webshop.webshopfullstack.persistence;

import com.webshop.webshopfullstack.persistence.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    Page<ProductEntity> findByCategoryId(int categoryId, Pageable pageable);

    Optional<ProductEntity> findById(long id);

    //TODO: create endpoints for this feature
    Page<ProductEntity> findByNameContaining(String name, Pageable pageable);
}
