package com.webshop.webshopfullstack.persistence;


import com.webshop.webshopfullstack.persistence.entity.RegistrationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RequestParam;


public interface RegistrationRepository extends CrudRepository<RegistrationEntity, Long> {

    RegistrationEntity findByEmail(String email);

    RegistrationEntity findByEmailAndPassword(String email, String password);

    RegistrationEntity findByAccessToken(String accessToken);

    RegistrationEntity findByAccessTokenAndEmail(String accessToken, String email);
}
