package com.webshop.webshopfullstack.service;

import com.webshop.webshopfullstack.persistence.RegistrationRepository;
import com.webshop.webshopfullstack.persistence.entity.RegistrationEntity;
import com.webshop.webshopfullstack.service.domain.Registration;
import org.apache.logging.log4j.util.Strings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

@Service
public class RegistrationService {

    private final RegistrationRepository registrationRepository;

    private final SecureRandom secureRandom;
    private final Base64.Encoder base64Encoder;

    public RegistrationService(RegistrationRepository registrationRepository, SecureRandom secureRandom, Base64.Encoder base64Encoder) {
        this.registrationRepository = registrationRepository;
        this.base64Encoder = base64Encoder;
        this.secureRandom = secureRandom;
    }


    public Boolean checkEmailExist(String email) {
        return registrationRepository.findByEmail(email) != null;
    }

    public Registration createUser(Registration registration) {
        if (validateRegistrationData(registration)) {
            throw new IllegalArgumentException("Not all required information was provided");
        }

        Registration registrationUser = new Registration(
                generateCustomerId(),
                registration.getFirstName(),
                registration.getLastName(),
                registration.getEmail(),
                securePassword(registration.getPassword(), "10"));

        RegistrationEntity entity = registrationRepository.save(
                mapToEntity(registrationUser)
        );

        return mapToDomain(entity);
    }

    private boolean validateRegistrationData(Registration registration) {
        return (Strings.isBlank(registration.getFirstName()) ||
                Strings.isBlank(registration.getLastName()) ||
                Strings.isBlank(registration.getEmail()) ||
                Strings.isBlank(registration.getPassword())
        );
    }

    private RegistrationEntity mapToEntity(Registration registration) {
        return new RegistrationEntity(
                registration.getCustomerId(),
                registration.getFirstName(),
                registration.getLastName(),
                registration.getEmail(),
                registration.getPassword()
        );

    }

    private Registration mapToDomain(RegistrationEntity registrationEntity) {
        return new Registration(
                registrationEntity.getCustomerId(),
                registrationEntity.getFirstName(),
                registrationEntity.getLastName(),
                registrationEntity.getEmail(),
                registrationEntity.getPassword()
        );
    }

    public static String securePassword(String passwordToHash, String salt) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes(StandardCharsets.UTF_8));
            byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    public Boolean validUser(String accessToken, String email) {
        return registrationRepository.findByAccessTokenAndEmail(accessToken, email) != null;
    }

    public RegistrationEntity addAccessTokenByEmail(String email) {
        var registrationEntity = registrationRepository.findByEmail(email);
        registrationEntity.setAccessToken(AuthService.generateToken());
        return registrationRepository.save(registrationEntity);
    }

    public String generateCustomerId() {
        byte[] randomBytes = new byte[12];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }

}
