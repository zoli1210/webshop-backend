package com.webshop.webshopfullstack.service;

import com.webshop.webshopfullstack.persistence.OrderRepository;
import com.webshop.webshopfullstack.persistence.RegistrationRepository;
import com.webshop.webshopfullstack.persistence.entity.OrderEntity;
import com.webshop.webshopfullstack.persistence.entity.OrderItemEntity;
import com.webshop.webshopfullstack.service.domain.Order;
import com.webshop.webshopfullstack.service.domain.Product;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderService {

    private final OrderRepository orderRepository;
    private final ProductService productService;

    public OrderService(OrderRepository orderRepository, ProductService productService) {
        this.orderRepository = orderRepository;
        this.productService = productService;
    }

    @Transactional
    public Order createOrder(Order submission) throws Exception {

        if (validateOrderSubmission(submission)) {
            throw new IllegalStateException("Not all required fields are filled");
        }

        String orderTrackingNumber = generateOrderTrackingNumber();
        submission.setOrderTrackingNumber(orderTrackingNumber);

        List<Order.Item> orderItems = submission.getOrderItems();
        List<OrderItemEntity> itemEntities = new ArrayList<>();

        for (Order.Item orderItem : orderItems) {
            Optional<Product> product = productService.getProductById(orderItem.getId());
            if (product.isEmpty()) {
                throw new IllegalArgumentException("Order products are empty");
            }
            var foundProduct = product.get();
            itemEntities.add(new OrderItemEntity(
                            orderItem.getId(),
                            foundProduct.getName(),
                            foundProduct.getImageUrl(),
                            foundProduct.getUnitPrice(),
                            orderItem.getQuantity()
                    )
            );
        }

        OrderEntity entity = orderRepository.save(
                mapToEntity(submission, itemEntities)
        );

        return mapToDomain(entity);
    }

    private boolean validateOrderSubmission(Order submission) {
        return !(
                Strings.isBlank(submission.getFirstName()) ||
                        Strings.isBlank(submission.getLastName()) ||
                        Strings.isBlank(submission.getStreetAndHouseNumber()) ||
                        Strings.isBlank(submission.getEmail()) ||
                        Strings.isBlank(submission.getCountry()) ||
                        Strings.isBlank(submission.getCity()) ||
                        Strings.isBlank(submission.getZipCode()) ||
                        submission.getOrderItems() != null ||
                        submission.getTotalPrice() == 0 ||
                        submission.getTotalQuantity() == 0 ||
                        submission.getOrderTrackingNumber() != null

        );
    }

    private String generateOrderTrackingNumber() {
        return UUID.randomUUID().toString();
    }

    private OrderEntity mapToEntity(Order order, List<OrderItemEntity> orderItems) {
        return new OrderEntity(
                order.getOrderTrackingNumber(),
                order.getFirstName(),
                order.getLastName(),
                order.getEmail(),
                order.getCountry(),
                order.getZipCode(),
                order.getCity(),
                order.getStreetAndHouseNumber(),
                order.getOther(),
                order.getCustomerId(),
                order.getTotalPrice(),
                order.getTotalQuantity(),
                order.getPaymentMethod(),
                orderItems
        );
    }

    private Order mapToDomain(OrderEntity orderEntity) {
        return new Order(

                orderEntity.getOrderTrackingNumber(),
                orderEntity.getFirstName(),
                orderEntity.getLastName(),
                orderEntity.getEmail(),
                orderEntity.getCountry(),
                orderEntity.getZipCode(),
                orderEntity.getCity(),
                orderEntity.getStreetAndHouseNumber(),
                orderEntity.getOther(),
                orderEntity.getCustomerId(),
                orderEntity.getTotalPrice(),
                orderEntity.getTotalQuantity(),
                orderEntity.getPaymentMethod(),
                listMappingToDomain(orderEntity.getOrderItems())
        );
    }

    private List<Order.Item> listMappingToDomain(List<OrderItemEntity> orderEntities) {
        List<Order.Item> itemList = new ArrayList<>();
        for (OrderItemEntity orderItem : orderEntities) {
            Order.Item orderItems = orderItemMapper(orderItem);
            itemList.add(orderItems);
        }
        return itemList;
    }

    private Order.Item orderItemMapper(OrderItemEntity newDomainItem) {
        return new Order.Item(
                newDomainItem.getUid(),
                newDomainItem.getQuantity(),
                newDomainItem.getName(),
                newDomainItem.getImageUrl(),
                newDomainItem.getUnitPrice()
        );
    }
}
