package com.webshop.webshopfullstack.service;


import com.webshop.webshopfullstack.persistence.RegistrationRepository;
import com.webshop.webshopfullstack.persistence.entity.RegistrationEntity;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Base64;

@Service
public class AuthService {

    private final RegistrationRepository registrationRepository;
    private static final SecureRandom secureRandom = new SecureRandom();
    private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder();

    public AuthService(RegistrationRepository registrationRepository) {
        this.registrationRepository = registrationRepository;
    }

    public Boolean checkValidUser(String email, String password) {

        var hashedPassword = RegistrationService.securePassword(password, "10");

        return registrationRepository.findByEmailAndPassword(email, hashedPassword) != null;
    }

    public Boolean checkCookieData(String accessToken) {

        return registrationRepository.findByAccessToken(accessToken) != null;
    }

    public static String generateToken() {
        byte[] randomBytes = new byte[24];
        secureRandom.nextBytes(randomBytes);
        return base64Encoder.encodeToString(randomBytes);
    }

}

