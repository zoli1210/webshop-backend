package com.webshop.webshopfullstack.service;

import com.webshop.webshopfullstack.persistence.ProductRepository;
import com.webshop.webshopfullstack.persistence.entity.ProductEntity;
import com.webshop.webshopfullstack.service.domain.Category;
import com.webshop.webshopfullstack.service.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getProducts() {
        return productRepository.findAll()
                .stream()
                .map(this::mapToDomain)
                .toList();
    }

    public Map<Category, List<Product>> getProductsGroupByCategories() {
        return productRepository.findAll()
                .stream()
                .map(this::mapToDomain)
                .collect(Collectors.groupingBy(Product::getCategory));
    }

    public List<Product> getProductsByCategory(int categoryId) {
        return productRepository.findByCategoryId((categoryId), Pageable.ofSize(10))
                .stream()
                .map(this::mapToDomain)
                .toList();
    }

    public Optional<Product> getProductById(long id) {
        return productRepository.findById(id).map(this::mapToDomain);
    }

    private Product mapToDomain(ProductEntity entity) {
        return new Product(
                entity.getId(),
                mapToCategory(entity.getCategoryId()),
                entity.getSku(),
                entity.getDescription(),
                entity.getName(),
                entity.getUnitPrice(),
                entity.getImageUrl(),
                entity.isActive(),
                entity.getUnitsInStock(),
                entity.getDateCreated(),
                entity.getLastUpdated());
    }

    private Category mapToCategory(int categoryId) {
        return switch (categoryId) {
            case 1 -> Category.BOOKS;
            case 2 -> Category.COFFEE_MUGS;
            case 3 -> Category.MOUSE_PADS;
            case 4 -> Category.LUGGAGE_TAGS;
            default -> throw new IllegalStateException(String.format("Unsupported category found: %d", categoryId));
        };
    }
}
