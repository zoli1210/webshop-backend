package com.webshop.webshopfullstack.service.domain;

import com.webshop.webshopfullstack.persistence.entity.RegistrationEntity;
import lombok.*;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
@Setter
public class Order {

    public Order() {
    }

    private String orderTrackingNumber;
    private String firstName;
    private String lastName;
    private String email;
    private String country;
    private String zipCode;
    private String city;
    private String streetAndHouseNumber;
    private String other;
    private String customerId;
    private int totalPrice;
    private int totalQuantity;
    private String paymentMethod;
    private List<Order.Item> orderItems;

    @AllArgsConstructor
    @Getter
    public static class Item {

        public Item(long id, int quantity) {
            this.id = id;
            this.quantity = quantity;
        }

        private Long id;
        private int quantity;
        private String name;
        private String imageUrl;
        private int unitPrice;
    }
}