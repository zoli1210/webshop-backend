package com.webshop.webshopfullstack.service.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Product {

    private Long id;
    private Category category;
    private String sku;
    private String name;
    private String description;
    private int unitPrice;
    private String imageUrl;
    private boolean active;
    private int unitsInStock;
    private Date dateCreated;
    private Date lastUpdated;
}
