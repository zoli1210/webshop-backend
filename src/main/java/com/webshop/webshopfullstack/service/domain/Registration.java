package com.webshop.webshopfullstack.service.domain;

import com.webshop.webshopfullstack.persistence.entity.OrderEntity;
import com.webshop.webshopfullstack.persistence.entity.RegistrationEntity;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class Registration {

    public Registration() {
    }

    public Registration(String customerId, String firstName, String lastName, String email, String password) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    private Long id;
    private String customerId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}