package com.webshop.webshopfullstack.service.domain;


public enum Category {

    BOOKS("Books", 1),
    COFFEE_MUGS("Coffe Mugs", 2),
    MOUSE_PADS("Mouse Pads", 3),
    LUGGAGE_TAGS("Luggage Tags", 4);

    private final String description;
    private final int id;

    Category(String description, int id) {
        this.description = description;
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }
}
