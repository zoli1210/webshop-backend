package com.webshop.webshopfullstack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebshopFullstackApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebshopFullstackApplication.class, args);
	}

}
